//
//  ApiClient.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/29.
//

import Foundation
import RxSwift
import Alamofire

fileprivate enum ApiError: Error, Equatable {
    case invaildResponse
    case requestError(code: String)
    case unreachable
}

fileprivate enum ApiResult<T> {
    case success(T?)
    case error(ApiError)
}

class ApiClient {
    class func getAssets(query model: AssetsQueryModel) -> Single<AssetsModel> {
        return fetch(.getAssets(query: model))
    }
    
    class func getBalance(address: String) -> Single<BalanceModel> {
        return fetch(.getBalance(address: address))
    }
}

private extension ApiClient {
    class func fetch<T: Codable>(_ request: ApiRouter, takeoverError: Bool = false) -> Single<T> {
        return Single<T>.create { single in
            self.request(request) { (response: ApiResult<T>?) in
                switch response {
                case .success(let result):
                    if let result = result {
                        single(.success(result))
                    } else {
                        single(.failure(ApiError.invaildResponse))
                    }
                case .error(let error):
                    single(.failure(error))
                default:
                    break
                }
            }
            return Disposables.create()
        }
    }
    
    class func request<T: Codable>(_ urlRequest: ApiRouter, completionHandler: @escaping (ApiResult<T>) -> Void) {
        AF.request(urlRequest).validate(statusCode: 200...299)
            .response { (response: AFDataResponse<Data?>) in
                switch response.result {
                case .success(let data):
                    guard let data = data else {
                        completionHandler(.success(nil))
                        return
                    }
                    do {
                        let object = try JSONDecoder().decode(T.self, from: data)
                        completionHandler(.success(object))
                    } catch {
                        completionHandler(.error(ApiError.invaildResponse))
                    }
                case .failure(_):
                    let code = response.response?.statusCode ?? -9999
                    completionHandler(.error(ApiError.requestError(code: String(code))))
                }
            }
    }
}

