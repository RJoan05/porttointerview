//
//  ApiRouter.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/29.
//

import Foundation
import Alamofire

// MARK: - Api Header
fileprivate enum HttpHeaderType: String {
    case contentType = "Content-Type"
}

fileprivate enum ContentType: String {
    case json = "application/json"
}

fileprivate enum ApiPath: String {
    case assets = "/api/v1/assets"
    case balance = "/addr/b/eth/"
    
    var path: String {
        return self.rawValue
    }
}

// MARK: - ApiUseType ( path, header )
enum ApiUseType {
    case assets
    case balance(address: String)
    
    var path: String {
        switch self {
        case .assets:
            return ApiPath.assets.path
        case .balance(let address):
            return ApiPath.balance.path + address
        }
    }
    
    var header: RouterFieldDictionary {
        var dic: [String: String] = [:]
        dic[HttpHeaderType.contentType.rawValue] = contentType
        return dic
    }
    
    var contentType: String {
        return ContentType.json.rawValue
    }
}

typealias RouterFieldDictionary = [String: String]

// MARK: - ApiRouter
struct ApiRouter: URLRequestConvertible {
    var domain: String = "https://testnets-api.opensea.io"
    let useType: ApiUseType
    let method: HTTPMethod
    var queryDicts: RouterFieldDictionary? = nil
    var parameterEncoding: ParameterEncoding = URLEncoding.default
    var customHeader: RouterFieldDictionary? = nil
    var parameters: Parameters? = nil
 
    func asURLRequest() throws -> URLRequest {
        let url = try (domain + useType.path).queryString(queryDicts).asURL()
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue
        urlRequest.timeoutInterval = 30
        
        if let cHeader = customHeader {
            urlRequest.allHTTPHeaderFields = cHeader
        } else {
            urlRequest.allHTTPHeaderFields = useType.header
        }
        
        return try parameterEncoding.encode(urlRequest, with: parameters)
    }
}
 
extension ApiRouter {
    // MARK: app launch
    static func getAssets(query: AssetsQueryModel) -> ApiRouter {
        return ApiRouter(useType: .assets, method: .get, queryDicts: query.convertToQueryDictionary())
    }
    
    static func getBalance(address: String) -> ApiRouter {
        return ApiRouter(domain: "http://www.tokenview.com:8088", useType: .balance(address: address), method: .get)
    }
}

// MARK: - extension
fileprivate extension String {
    func queryString(_ queryDict: [String : String]?) -> String {
        guard let dict = queryDict, dict.count > 0 else {
            return self
        }
        
        var suffix: String = "?"
        if self.contains("?"), self.contains("=") { suffix = "&" }
        
        var queryURL = self + suffix
        for (index, val) in dict.enumerated() {
            let qValue = val.value.urlQueryEncoded() ?? val.value
            queryURL += val.key + "=" + qValue
            if index < dict.count - 1 {
                queryURL += "&"
            }
        }
        
        return queryURL
    }
    
    func urlQueryEncoded(denying deniedCharacters: CharacterSet = .urlQueryParameterNotAllowed) -> String? {
        return addingPercentEncoding(withAllowedCharacters: .urlQueryParameterNotAllowed)
    }
}

fileprivate extension CharacterSet {
    static let urlQueryParameterNotAllowed = CharacterSet.urlQueryAllowed.subtracting(CharacterSet(charactersIn: "!#$%&'()*+,/:;=?@[]"))
}
