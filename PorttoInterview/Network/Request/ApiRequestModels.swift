//
//  ApiRequestModels.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/29.
//

import Foundation
import Alamofire

// MARK: - protocol Parameter
protocol ParameterConvertible {
    func convertToParameter() -> Parameters?
    func convertToQueryDictionary() -> RouterFieldDictionary?
}
 
extension ParameterConvertible where Self: Encodable {
    
    func convertToParameter() -> Parameters? {
        do {
            let data = try JSONEncoder().encode(self)
            return try JSONSerialization.jsonObject(with: data, options: []) as? Parameters
        } catch {
            fatalError("\(error)")
        }
    }
    
    func convertToQueryDictionary() -> RouterFieldDictionary? {
        do {
            let data = try JSONEncoder().encode(self)
            if let result = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] {
                var dict: RouterFieldDictionary = [:]
                result.forEach {
                    let value = stringFromAny($0.value)
                    guard !value.isEmpty else {
                        return
                    }
                    dict[$0.key] = value
                }
                
                return dict
            } else {
                return nil
            }
        } catch {
            fatalError("\(error)")
        }
    }
    
    func stringFromAny(_ value: Any?) -> String {
        guard let nonNil = value, !(nonNil is NSNull) else {
            return ""
        }
        
        return String(describing: nonNil)
    }
    
}

struct AssetsQueryModel: Codable, ParameterConvertible {
    let owner: String
    var order_direction: String = "desc"
    let offset: Int
    var limit: Int = 20
}
