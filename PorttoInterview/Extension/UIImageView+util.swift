//
//  UIImageView+util.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/29.
//

import UIKit
import Kingfisher

extension UIImageView {
    fileprivate enum AssetTyep: String {
        case svg
        case normal
    }

    func setImage(imageURL: String?) {
        guard let path = imageURL, !path.isEmpty else {
            self.image = nil
            return
        }
                
        let fileTyep = path.components(separatedBy: ".").last
        let type: AssetTyep = AssetTyep(rawValue: fileTyep ?? "") ?? .normal
        
        guard let url = URL(string: path) else {
            self.image = nil
            return
        }
        
        let completionHandler: (Result<RetrieveImageResult, KingfisherError>) -> Void = { result in
            switch result {
            case .success(let retrieve):
                self.image = self.resizeImage(image: retrieve.image, newWidth: self.bounds.width)
            case .failure(_):
                self.image = nil
            }
        }
        
        guard type == .normal else {
            self.kf.setImage(with: url, options: [.processor(SVGImgProcessor())], completionHandler: completionHandler)
            return
        }
        
        self.kf.setImage(with: url, completionHandler: completionHandler)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage ?? UIImage()
    }
}
