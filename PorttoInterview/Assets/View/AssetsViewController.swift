//
//  AssetsViewController.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/29.
//

import UIKit
import SnapKit
import RxSwift

class AssetsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var viewModel: AssetsViewModel!
    var disposeBag = DisposeBag()
    
    private lazy var balanceLabel: UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.textAlignment = .left
        return lbl
    }()
    
    private lazy var collectionView: UICollectionView = {
        let width = (view.bounds.width - 20) / 2
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = CGSize(width: width, height: width)
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.register(AssetCollectionCell.self, forCellWithReuseIdentifier: "AssetCollectionCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        return collectionView
    }()
    
    static func initVC(with vm: AssetsViewModel) -> AssetsViewController {
        let vc = AssetsViewController()
        vc.viewModel = vm
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(balanceLabel)
        view.addSubview(collectionView)
        
        balanceLabel.snp.makeConstraints { make in
            make.top.equalTo(view.safeAreaLayoutGuide).offset(8)
            make.leading.equalToSuperview().offset(8)
            make.centerX.equalToSuperview()
        }
        
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(balanceLabel.snp.bottom).offset(8)
            make.leading.trailing.bottom.equalToSuperview()
        }
        
        initBinding()
        viewModel.fetchBalance()
        viewModel.fetchAssetsData()
    }
    
    func initBinding() {
        viewModel.assetsListDidChange.observe(on: MainScheduler.instance).subscribe { _ in
            self.collectionView.reloadData()
        }.disposed(by: disposeBag)
        
        viewModel.userBalance.bind(to: balanceLabel.rx.text).disposed(by: disposeBag)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AssetCollectionCell", for: indexPath) as! AssetCollectionCell
        
        let cellViewModel = viewModel.data(forRowAt: indexPath.row)
        cell.config(cellViewModel)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == viewModel.numberOfRows() - 4 && !viewModel.isLoading {
            viewModel.fetchAssetsData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.showAsset(at: indexPath.row)
    }
}
