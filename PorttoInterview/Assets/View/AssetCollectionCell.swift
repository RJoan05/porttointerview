//
//  AssetCollectionCell.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/29.
//

import UIKit
import Kingfisher

class AssetCollectionCell: UICollectionViewCell {
    
    var icon: UIImageView = {
        let iView = UIImageView()
        iView.contentMode = .scaleAspectFit
        return iView
    }()
    
    var nameLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "sssss"
        lbl.textAlignment = .center
        return lbl
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        // initialize what is needed
        setupView()
    }
    
    private func setupView() {
        contentView.addSubview(icon)
        contentView.addSubview(nameLabel)
        
        icon.snp.makeConstraints { make in
            make.top.leading.equalTo(10)
            make.trailing.equalTo(-10)
            make.height.equalTo(icon.snp.width)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.leading.trailing.equalTo(icon)
            make.bottom.equalToSuperview().offset(-10)
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        icon.image = nil
        nameLabel.text = ""
    }
    
    func config(_ viewModel: AssetCellViewModel) {
        nameLabel.text = viewModel.name
        icon.setImage(imageURL: viewModel.imageURL)
    }
}
