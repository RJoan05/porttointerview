//
//  AssetCellViewModel.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/29.
//

import Foundation

struct AssetCellViewModel {
    let imageURL: String?
    let name: String
}
