//
//  AssetsViewModel.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/29.
//

import Foundation
import RxSwift
import RxCocoa

class AssetsViewModel {
    private(set) var isLoading: Bool = false
    private var disposeBag = DisposeBag()
    private let interactor: AssetsInteractorProtocol
    private let coordinator: AssetsCoordinatorProtocol
    let assetsListDidChange = PublishSubject<Void>()
    let userBalance: BehaviorRelay<String> = BehaviorRelay(value: "")
    
    private var assetsList: [AssetModel] {
        return interactor.assetsList
    }
    
    init(interactor: AssetsInteractorProtocol, coordinator: AssetsCoordinatorProtocol) {
        self.interactor = interactor
        self.coordinator = coordinator
        self.initBinding()
    }
    
    func fetchBalance() {
        interactor.fetchBalance()
    }
    
    func fetchAssetsData() {
        isLoading = true
        interactor.fetchAssetsData()
    }
    
    func data(forRowAt index: Int) -> AssetCellViewModel {
        let asset = assetsList[index]
        return AssetCellViewModel(imageURL: asset.thumbnailURL, name: asset.name ?? "")
    }
    
    func numberOfRows() -> Int {
        return assetsList.count
    }
    
    func showAsset(at index: Int) {
        coordinator.present(asset: assetsList[index])
    }
    
    private func initBinding() {
        interactor.assetsListDidChange.bind(to: assetsListDidChange).disposed(by: disposeBag)
        interactor.assetsListDidChange.subscribe { _ in
            self.isLoading = false
        }.disposed(by: disposeBag)
        
        interactor.userBalance.map {
            guard !$0.isEmpty else {
                return ""
            }
            return "餘額: \($0)"
        }.bind(to: userBalance).disposed(by: disposeBag)
    }
}
