//
//  AssetsInteractor.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/29.
//

import Foundation
import RxSwift
import RxRelay

protocol AssetsInteractorProtocol {
    var offset: Int { get }
    var assetsList: [AssetModel] { get }
    var assetsListDidChange: PublishSubject<Void> { get }
    var userBalance: BehaviorRelay<String> { get }
    
    func fetchAssetsData()
    func fetchBalance()
}

class AssetsInteractor: AssetsInteractorProtocol {
    
    private var disposeBag = DisposeBag()
    
    private let userAddress: String = "0x19818f44faf5a217f619aff0fd487cb2a55cca65"
    private(set) var offset: Int = -1
    private(set) var assetsList: [AssetModel] = []
    let assetsListDidChange: PublishSubject<Void> = PublishSubject<Void>()
    let userBalance: BehaviorRelay<String> = BehaviorRelay(value: "")
    
    func fetchAssetsData() {
        offset += 1
        ApiClient.getAssets(query: AssetsQueryModel(owner: userAddress, offset: offset)).subscribe { assets in
            self.assetsList += assets.assets
            self.assetsListDidChange.onNext(())
        } onFailure: { _ in
            self.offset -= 1
        }.disposed(by: disposeBag)
    }
    
    func fetchBalance() {
        ApiClient.getBalance(address: userAddress).subscribe { balance in
            self.userBalance.accept(balance.data)
        } onFailure: { _ in
        }.disposed(by: disposeBag)
    }
}
