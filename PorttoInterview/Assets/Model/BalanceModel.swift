//
//  BalanceModel.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/30.
//

import Foundation

struct BalanceModel: Codable {
    let code: Int
    let msg: String
    let data: String
}
