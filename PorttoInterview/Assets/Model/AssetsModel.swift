//
//  AssetsModel.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/29.
//

import Foundation

struct AssetsModel: Codable {
    let assets: [AssetModel]
}

struct AssetModel: Codable {
    let id: Int
    let name: String?
    let imageURL: String?
    let thumbnailURL: String?
    let description: String?
    let permalink: String?
    let collection: CollectionModel
    
    enum CodingKeys: String, CodingKey {
        case id, name, description, permalink, collection
        case imageURL = "image_url"
        case thumbnailURL = "image_thumbnail_url"
    }
}

struct CollectionModel: Codable {
    let name: String
}
