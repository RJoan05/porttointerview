//
//  RootCoordinator.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/29.
//

import UIKit

protocol Coordinator {
    func start()
}

final class RootCoordinator: Coordinator {
    private var childCoordinators = [Coordinator]()
    private weak var navigationController: UINavigationController?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        guard let navigationController = navigationController else { return }
        let assetsCoordinator = AssetsCoordinator()
        let assetsInteractor = AssetsInteractor()
        let assetsViewModel = AssetsViewModel(interactor: assetsInteractor, coordinator: assetsCoordinator)
        let assetsViewController = AssetsViewController.initVC(with: assetsViewModel)
        assetsCoordinator.navigationController = navigationController
        navigationController.viewControllers = [assetsViewController]
    }
}
