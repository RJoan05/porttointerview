//
//  AssetsCoordinator.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/29.
//

import UIKit


protocol AssetsCoordinatorProtocol {
    func present(asset: AssetModel)
}

final class AssetsCoordinator: AssetsCoordinatorProtocol {
    
    weak var navigationController: UINavigationController?
    
    func present(asset: AssetModel) {
        let detailCoordinator = AssetDetailCoordinator()
        let detailViewModel = AssetDetailViewModel(coordinator: detailCoordinator, model: asset)
        let detailViewController = AssetDetailViewController.initVC(with: detailViewModel)
        detailCoordinator.navigationController = navigationController
        navigationController?.pushViewController(detailViewController, animated: true)
    }
    
}
