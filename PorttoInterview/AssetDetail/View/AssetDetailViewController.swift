//
//  AssetDetailViewController.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/29.
//

import UIKit
import RxSwift

class AssetDetailViewController: UIViewController {
    
    var disposeBag = DisposeBag()
    var viewModel: AssetDetailViewModel!
    
    lazy var scrollView: UIScrollView = {
        let sView = UIScrollView()
        sView.addSubview(contentView)
        return sView
    }()
    
    lazy var contentView: UIView = {
        let view = UIView()
        view.addSubview(assetImageView)
        view.addSubview(nameLabel)
        view.addSubview(descriptionLabel)
        return view
    }()
    
    lazy var assetImageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    lazy var nameLabel: UILabel = {
        let lbl = UILabel()
        lbl.textAlignment = .center
        return lbl
    }()
    
    lazy var descriptionLabel: UILabel = {
        let lbl = UILabel()
        lbl.numberOfLines = 0
        lbl.textAlignment = .left
        return lbl
    }()
    
    lazy var showLinkButton: UIButton = {
        var config = UIButton.Configuration.bordered()
        config.title = "permalink"
        config.titlePadding = 8
        config.baseBackgroundColor = .blue
        let button = UIButton.init(configuration: config, primaryAction: nil)
        button.configuration = config
        button.tintColor = .white
        return button
    }()
    
    static func initVC(with vm: AssetDetailViewModel) -> AssetDetailViewController {
        let vc = AssetDetailViewController()
        vc.viewModel = vm
        vc.title = vm.asset.collection.name
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(scrollView)
        view.addSubview(showLinkButton)
        
        scrollView.snp.makeConstraints { make in
            make.edges.equalTo(view.safeAreaInsets)
        }
        
        contentView.snp.makeConstraints { make in
            make.top.bottom.equalTo(scrollView)
            make.left.right.equalTo(view)
        }
        
        assetImageView.snp.makeConstraints { make in
            make.top.equalToSuperview()
            make.leading.equalToSuperview().offset(8)
            make.trailing.equalToSuperview().offset(-8)
        }
        
        nameLabel.snp.makeConstraints { make in
            make.leading.centerX.equalTo(assetImageView)
            make.top.equalTo(assetImageView.snp.bottom).offset(8)
        }
        
        descriptionLabel.snp.makeConstraints { make in
            make.leading.centerX.equalTo(assetImageView)
            make.top.equalTo(nameLabel.snp.bottom).offset(8)
            make.bottom.equalTo(contentView).inset(50)
        }
        
        showLinkButton.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide).offset(-8)
        }
        
        initBinding()
    }
    
    private func initBinding() {
        viewModel.name.bind(to: nameLabel.rx.text).disposed(by: disposeBag)
        viewModel.description.bind(to: descriptionLabel.rx.text).disposed(by: disposeBag)
        
        viewModel.imageURL.distinctUntilChanged().observe(on: MainScheduler.instance).subscribe { [unowned self] imageURL in
            guard let path = imageURL else {
                self.assetImageView.image = nil
                return
            }
            
            self.view.layoutIfNeeded()
            self.assetImageView.setImage(imageURL: path)
            self.assetImageView.contentMode = .scaleAspectFit
        } onError: { _ in
            
        }.disposed(by: disposeBag)
        
        showLinkButton.rx.controlEvent(.touchUpInside).observe(on: MainScheduler.instance).subscribe { [unowned self] _ in
            guard let permalink = self.viewModel.asset.permalink, let url = URL(string: permalink), UIApplication.shared.canOpenURL(url) else {
                return
            }
            
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }.disposed(by: self.disposeBag)

    }
}
