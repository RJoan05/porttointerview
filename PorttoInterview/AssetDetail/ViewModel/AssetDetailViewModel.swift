//
//  AssetDetailViewModel.swift
//  PorttoInterview
//
//  Created by ZoeLin on 2022/1/29.
//

import Foundation
import RxSwift
import RxRelay

protocol AssetsDetailViewModelProtocol {
    var asset: AssetModel { get }
    var imageURL: BehaviorRelay<String?> { get }
    var name: BehaviorRelay<String?> { get }
    var description: BehaviorRelay<String?> { get }
}

class AssetDetailViewModel: AssetsDetailViewModelProtocol {
    private(set) var asset: AssetModel
    let imageURL: BehaviorRelay<String?> = BehaviorRelay(value: nil)
    let name: BehaviorRelay<String?> = BehaviorRelay(value: nil)
    let description: BehaviorRelay<String?> = BehaviorRelay(value: nil)
    
    private let coordinator: AssetDetailCoordinator
    
    init(coordinator: AssetDetailCoordinator, model: AssetModel) {
        self.coordinator = coordinator
        self.asset = model
        self.name.accept(model.name)
        self.description.accept(model.description)
        self.imageURL.accept(model.imageURL)
    }
    
}
